package net.avcompris.examples.users3.web.it;

import static io.restassured.RestAssured.given;
import static net.avcompris.commons3.web.it.utils.JSONUtils.parseJSON;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

import io.restassured.response.Response;
import net.avcompris.commons3.api.exception.UnauthenticatedException;
import net.avcompris.commons3.web.ApplicationError;
import net.avcompris.commons3.web.it.utils.AbstractWebTest;
import net.avcompris.commons3.web.it.utils.RestAssured;
import net.avcompris.examples.users3.api.UsersInfo;

@RestAssured("${users.baseURL}")
public class UsersTest extends AbstractWebTest {

	@Test
	public void users_withAuthorization_returns_200_with_data() throws Exception {

		final Response response = given()

				.header("Authorization", "toto")

				.when().get("/api/v1/users")

				.then()

				.log().body()

				.statusCode(200)

				.extract().response();

		final String correlationId = response.getHeader("Correlation-ID");

		assertNotNull(correlationId, "correlationId should not be null");

		final UsersInfo users = parseJSON(response.asString(), UsersInfo.class);

		users.getTotal();
	}

	@Test
	public void users_withNoAuthorization_returns_401_with_error() throws Exception {

		final Response response = given()

				.when().get("/api/v1/users")

				.then()

				.statusCode(401)

				.extract().response();

		final String correlationId = response.getHeader("Correlation-ID");

		assertNotNull(correlationId, "correlationId should not be null");

		final ApplicationError error = parseJSON(response.asString(), ApplicationError.class);

		assertNotNull(error, "error should not be null");

		assertEquals(401, error.getStatusCode());
		assertEquals(correlationId, error.getCorrelationId());
		assertEquals(UnauthenticatedException.class.getSimpleName(), error.getType());
	}
}
